# xwallpaper
# Autogenerated from man page /usr/share/man/man1/xwallpaper.1.gz
complete -c xwallpaper -l center -d 'Centers the input file on the output'
complete -c xwallpaper -l clear -d 'Initializes screen with a black background'
complete -c xwallpaper -l daemon -d 'Keeps xwallpaper xwallpaper running in background, listening for RandR events'
complete -c xwallpaper -l debug -d 'Displays debug messages on the standard error output while xwallpaper xwallpa…'
complete -c xwallpaper -l focus -d 'In conjunction with trim the specified trim box will be guaranteed to be visi…'
complete -c xwallpaper -l maximize -d 'Maximizes input file to fit output without cropping'
complete -c xwallpaper -l no-atoms -d 'Atoms which are used for pseudo transparency are not updated'
complete -c xwallpaper -l no-randr -d 'Ignores individual outputs and uses the whole output instead'
complete -c xwallpaper -l no-root -d 'The background of the root window is not updated'
complete -c xwallpaper -l trim -d 'Specifies area of interest in source file'
complete -c xwallpaper -l output -d 'Specifies the output on which a subsequently supplied file shall be set as wa…'
complete -c xwallpaper -l screen -d 'Specifies a screen by its screen number'
complete -c xwallpaper -l stretch -d 'Stretches input file to fully cover the output'
complete -c xwallpaper -l tile -d 'Uses tiling mode'
complete -c xwallpaper -l version -d 'Prints version and exits'
complete -c xwallpaper -l zoom -d 'Zooms input file to fit output with cropping'

