# aura
# Autogenerated from man page /usr/share/man/man8/aura.8.gz
complete -c aura -s A -l aursync
complete -c aura -s B -l save
complete -c aura -s C -l downgrade
complete -c aura -s L -l viewlog
complete -c aura -s O -l orphans
complete -c aura -s P -l analysis
complete -c aura -s a -l delmakedeps
complete -c aura -s c -l clean
complete -c aura -s d -l deps
complete -c aura -s i -l info
complete -c aura -s k -l diff
complete -c aura -s p -l pkgbuild
complete -c aura -s q -l quiet
complete -c aura -s s -l search
complete -c aura -s u -l sysupgrade
complete -c aura -o Au -d 'is like'
complete -c aura -o Su -d 'but for AUR packages'
complete -c aura -s w -l downloadonly
complete -c aura -s x -l unsuppress
complete -c aura -l json
complete -c aura -l build
complete -c aura -l builduser
complete -c aura -l devel
complete -c aura -l dryrun
complete -c aura -s M
complete -c aura -l force
complete -c aura -l hotedit
complete -c aura -o Ap -o Ad -d 'for this, as they will be much faster at presenting information than searchin…'
complete -c aura -s E -d 'aura -A --hotedit . to preserve your environment'
complete -c aura -l skipdepcheck
complete -c aura -l vcspath
complete -c aura -s r -l restore
complete -c aura -s l -l list
complete -c aura -s b -l backup -d ' path'
complete -c aura -o Cc
complete -c aura -o Scc
complete -c aura -l notsaved
complete -c aura -l adopt
complete -c aura -s j -l abandon
complete -c aura -s f -l file -d ' path'
complete -c aura -l dir -d ' path'
complete -c aura -l audit
complete -c aura -l noconfirm
complete -c aura -l needed
complete -c aura -l debug
complete -c aura -l color
complete -c aura -l overwrite
complete -c aura -l ignorearch
complete -c aura -l allsource
complete -c aura -l allsourcepath -d 'flag on command line or via aura. conf (5)'
complete -c aura -l skipinteg
complete -c aura -l skippgpcheck
complete -c aura -o Ai -d and
complete -c aura -o Akua -d 'instead of just'

